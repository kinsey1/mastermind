#include "code_check.h"


/*copys something to send back to client into output,
returns -1 for invalid guess, 1 for winning guess and
0 for a valid guess but no win*/
int check_code(char* input, char* output, char* code){
    
    if(input[strlen(input)-1]=='\n'){
        input[strlen(input)-1]='\0';
    }
    
 
    int cor_pos;
    int cor_let;
    
    char buff[BUF_SIZE];
    memset(buff,0,BUF_SIZE);
    
    
    if(valid_code(input)==-1){
        strcpy(output, "INVALID\n");
        return -1;
    }
    if(char_pos(input, code) == 4){
        strcpy(output,"SUCCESS");
        return 1;
    }
    
    cor_pos = char_pos(input, code);
    cor_let = char_in(input, code);
    
    snprintf(buff,BUF_SIZE, "[%d:%d]", cor_pos, cor_let);
    strcpy(output,buff);
    
    
    return 0;
    
}

/*Gets the amount of
characters in the correct
position*/
int char_pos(char* input, char* code){
    int i;
    int correct = 0;
    
    
    for(i=0;i<CODE_SIZE;i++){
        if(input[i] == code[i]){
            correct++;
        }
    }
    return correct;
}

/*gets the amount of 
characters in the input
that are in the code*/

int char_in(char* input, char* code){
    int i, j;
    int correct = 0;
    
    char t_code[CODE_SIZE];
    strcpy(t_code, code);
    
    for(i=0; i<CODE_SIZE; i++){
        for(j=0; j<CODE_SIZE; j++){
            
            if(input[i]==t_code[j]){
                t_code[j] = '0';
                correct++;
                break;
            }
        }
    }
    return correct - char_pos(input, code);
    
}


/*Generates a 'random' code
within the mastermind boundaries
*/
char* gen_code(){
    time_t t;
    int i = 0;
    int ran;
    srand((unsigned) time(&t));
    char* c = (char*)malloc(4);
    
    while(i < 4){
        ran = rand() % 6;
        if(ran==0)c[i] = 'A';
        else if(ran==1)c[i] = 'B';
        else if(ran == 2)c[i] = 'C';
        else if(ran == 3)c[i] = 'D';
        else if(ran == 4)c[i] = 'E';
        else if(ran == 5)c[i] = 'F';
        i++;
    }
    return c;
    
}

/*
Returns -1 if a code is invalid, 1 if valid.
*/
int valid_code(char* code){
    int i;
    if(code[strlen(code)-1]=='\n'){
        code[strlen(code)-1]='\0';
    }
    if((strlen(code))!=CODE_SIZE)return -1;
    for(i=0;i < CODE_SIZE; i++){
        if(valid_char(code[i])==-1){
            return -1;
        }
    }
    return 1;
}

/*checks if a char is valid, 1 valid, -1 invalid*/
int valid_char(char c){
    if(c=='A' || c== 'B' || c== 'C' || c == 'D' || c == 'E' || c == 'F'){
        return 1;
    }
    return -1;
}