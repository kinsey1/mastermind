#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <pthread.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/resource.h>

#include "code_check.h"


#define PORT 4444
#define BUF_WEL 100
#define BUF_SIZE 100
#define CLADDR_LEN 100
#define CODE_SIZE 4
#define MSG_BUF 400
#define MAX_GUESS 10

void* receive(void *args);
void server_setup(int port, char* code);
void write_stats();
void write_log(int cli_no, char* cli_ip, int sock_id, char* message, int type);
void interrupt_handle();

struct arg_struct {
    char* addr;
    int socket;
    char* code;
    int cli_no;
};

typedef struct arg_struct arg_t;

pthread_mutex_t lock;

int num_threads = 0;
int num_c_guess = 0;

