#include <stdio.h>
#include "stdlib.h"  
#include "sys/types.h"  
#include "sys/socket.h"  
#include "string.h"  
#include "netinet/in.h"  
#include "netdb.h"
#include "pthread.h"
#include <unistd.h>
#include "code_check.h"




#define PORT 4444 
#define BUF_SIZE 100
#define MSG_BUF 400
#define CODE_SIZE 4

void * receive_message(void * socket);
void client_setup(char* serv_addr, char* port);
int game_state(char* input);
