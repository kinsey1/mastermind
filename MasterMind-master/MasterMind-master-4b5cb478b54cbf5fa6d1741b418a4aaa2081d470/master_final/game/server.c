#include "server.h"

/*Main server file, accepts new clients and 
creates a new thread for each.*/

int main(int argc, char* argv[]) {
    
    


    signal(SIGINT, interrupt_handle);
    if(argc < 2){
        printf("not enough arguments");
        exit(1);
    }else if(argc==3){
        int port = atoi(argv[1]);
        server_setup(port, argv[2]);
    }else{
        int port = atoi(argv[1]);
        server_setup(port, gen_code());
    }
    return 0;
}



/*Message recieve handler
receives a message and sends 
back an appropriate message.
This is the worker function
for every new thread created*/
void * receive(void *args) {
    
    num_threads++;
    arg_t *arg = args;
    int ret;
    int guesses = 0;
    char buffer[BUF_SIZE];
    char output[BUF_SIZE];
    char message[BUF_SIZE];
    
    
    memset(buffer, 0, BUF_SIZE);
    memset(output, 0, BUF_SIZE);
    
    while(1) {
        memset(buffer, 0, BUF_SIZE);
        memset(output, 0, BUF_SIZE);
        memset(message, 0, BUF_SIZE);
        
        ret = read(arg->socket, buffer, BUF_SIZE);
        if (ret <= 0) {
            snprintf(message,BUF_SIZE, "Client disconnected, closing client!");
            write_log(arg->cli_no, arg->addr, arg->socket, NULL, 5);
            printf("Client %s quit unexpectedly\n", arg->addr);
            fflush(stdout);
            close(arg->socket);
            break;
        } else {

            if(strlen(buffer)>1){
                
                printf("client (%s): ", arg->addr);
                printf("%s\n", buffer);
                int out = check_code(buffer, output, arg->code);
                write_log(arg->cli_no, arg->addr, arg->socket, buffer, 1);
                guesses++;
                
                //Client guessed successfully
                if(out == 1){
                    write_log(arg->cli_no, arg->addr, arg->socket, NULL, 3);
                    
                    num_c_guess++;
                    write(arg->socket, output, BUF_SIZE);
                    close(arg->socket);
                    break;
                    
                //Client guessed too many times
                }else if(guesses >= MAX_GUESS){
                    write_log(arg->cli_no, arg->addr, arg->socket, NULL, 4);
                    
                    snprintf(message,
                    BUF_SIZE,
                     "FAILURE! No more attempts. The code was %s.",
                     arg->code);
                     
                    write(arg->socket, message, BUF_SIZE);
                    close(arg->socket);
                    break;
                    
                //client needs to keep guessing
                }else if(out == 0){
                    write_log(arg->cli_no, arg->addr, arg->socket, output, 2);
                    
                    snprintf(message,
                    BUF_SIZE,
                     " %d guesses left.",
                      MAX_GUESS-guesses);
                    
                    strcat(output, message);
                    write(arg->socket, output, BUF_SIZE);
                    
                //client gave an invalid input   
                }else if(out == -1){
                    write_log(arg->cli_no, arg->addr, arg->socket, output, 2);
                    
                    snprintf(message,
                    BUF_SIZE, 
                    "Take another guess, you have %d guesses.",
                     MAX_GUESS-guesses);
                        
                    strcat(output, message);
                    write(arg->socket, output, BUF_SIZE);
                }
                
            }


        }
    }
    return NULL;
}



/*Sets up the server and handles
multiple client connections.
Takes the port and code as inputs
*/
void server_setup(int port, char* code){
    printf("Welcome to the Mastermind Server, waiting for connections\n");
    fflush(stdout);
    
    FILE* log_fp;
    log_fp = fopen("log.txt", "w+");
    fclose(log_fp);
    
    
    struct sockaddr_in s_addr, cl_addr;
    int sockfd, len, ret, newsockfd;
    char buffer[BUF_SIZE];
    //pid_t childpid;
    char clientAddr[CLADDR_LEN];

    

    memset( &s_addr, 0, sizeof(s_addr));
    memset( &s_addr, 0, sizeof(s_addr));
    
    
    
    s_addr.sin_family = AF_INET;
    s_addr.sin_addr.s_addr = INADDR_ANY;
    s_addr.sin_port = htons(port);
    
    
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printf("Error creating socket!\n");
        exit(1);
    }
    
    ret = bind(sockfd, (struct sockaddr * ) & s_addr, sizeof(s_addr));
    if (ret < 0) {
        printf("Error binding!\n");
        exit(1);
    }
    
    
    listen(sockfd, 5);

    len = sizeof(cl_addr);
    int num_con = 0;
    while(newsockfd = accept(sockfd, (struct sockaddr * ) & cl_addr, & len)){
        
        
        num_con++;
        //accept multiple cliet connections.
        int* new_sock;
        //printf("new connection accepted, code is %s\n", code);
        if (newsockfd < 0) {
            printf("Error accepting connection!\n");
            exit(1);
        }
        
        char* message = "Welcome to mastermind. Guess a 4 letter string to \
begin. Allowed characters: {A, B, C, D, E, F}. You will be prompted by a hint,\
in the form of [number of letters in the correct position: number of  \
letters guessed in the code]\n";
        
        write(newsockfd , message , MSG_BUF);
    
        //converts the client address (clientAddr) to a string.
        inet_ntop(AF_INET, & (cl_addr.sin_addr), clientAddr, CLADDR_LEN);
        
        write_log(newsockfd, clientAddr, num_con, NULL, 0);
        
        memset(buffer, 0, BUF_SIZE);
        
        pthread_t worker_thread;
        new_sock = malloc(1);
        *new_sock = newsockfd;
    
        arg_t *args = malloc(sizeof(arg_t)); 
    
        args->addr = malloc(sizeof(clientAddr));
        args->socket = newsockfd;
        args->code = code;
        args->cli_no = num_con;
        
        strcpy(args->addr, clientAddr);
    
        ret = pthread_create( & worker_thread, NULL, receive, args);
    }

    if (ret) {
        printf("ERROR: Return Code from pthread_create() is %d\n", ret);
        exit(1);
    }
    
    while(1);

    close(newsockfd);
    close(sockfd);

    pthread_exit(NULL);
}




/*
The type arguments is for type of line to write to the file,
0 = client connected.
1 = client guess.
2 = server hint.
3 = Success
4 = Failure
Takes client number,

*/
void write_log(int cli_no, char* cli_ip, int sock_id, char* message, int type){
    time_t raw_time;
    struct tm* timeinfo;
    time(&raw_time);
    timeinfo = localtime(&raw_time);
       
    FILE* log_fp;
    pthread_mutex_lock(&lock);
    log_fp = fopen("log.txt", "a");
    if(type == 0){
        fprintf(log_fp,
         "Connection from %s,(%d)\n",
          cli_ip,
          sock_id);
    }
    else if(type == 1){
        fprintf(log_fp, 
        "Time %s - %s, (Client number: %d), (Sock: %d), Clients guess :%s\n",
        asctime(timeinfo),
        cli_ip, cli_no,
        sock_id,
        message);
        
    }else if(type==2){
        
        fprintf(log_fp,
         "Time %s - %s, (%d), (sock: %d), Servers hint :%s\n",
         asctime(timeinfo),
         cli_ip,
         cli_no,
         sock_id,
         message);
    }else if(type==3){
        
        fprintf(log_fp,
         "Time %s - %s, (%d), (sock: %d), SUCCESS\n",
         asctime(timeinfo),
         cli_ip,
         cli_no,
         sock_id);
         
    }else if(type == 4){
        
        fprintf(
        log_fp,
        "Time %s - %s, (%d), (sock: %d), FAILURE\n",
        asctime(timeinfo), 
        cli_ip, 
        cli_no, 
        sock_id);
    }else if(type == 5){
        fprintf(log_fp, 
        "Time %s - %s, (%d), (sock: %d) Disconnected unexpectedly. \n",
        asctime(timeinfo), 
        cli_ip, 
        cli_no, 
        sock_id);
    }
    fclose(log_fp);
    pthread_mutex_unlock(&lock);
    
}


/*handles the interrupts.
writes some stats about the server
to the log file. It uses a set of bash
commands to write to the file
*/
void interrupt_handle(){
    
    int pid = getpid();
    char command[500];
    struct rusage r_usage;
    FILE* fp_log=fopen("log.txt", "a");
    fprintf(fp_log, "\n%d clients connected, %d guessed correctly\n",num_threads, num_c_guess);
    
    getrusage(RUSAGE_SELF,&r_usage);
    fprintf(fp_log, "Total Memory usage by process = %ldkb\n",r_usage.ru_maxrss);
    
    sprintf(command, "echo Resident set size KiB: >> log.txt\n");
    system(command);
    sprintf(command, "cat /proc/%d/status | grep VmRSS >>log.txt\n", pid);
    system(command);
    
    
    sprintf(command, "echo Server running time hh:mm:ss- : >> log.txt\n");
    system(command);
    
    sprintf(command, "ps -p %d -o etime= >> log.txt",pid);
    system(command);
    
    
    sprintf(command, "echo Priority: >> log.txt\n");
    system(command);
    sprintf(command, "cat /proc/%d/stat | awk '{print $18}'>>log.txt\n", pid);
    system(command);
    
    
    sprintf(command, "echo Number of min faults, not requiring page load from disk : >> log.txt\n");
    system(command);
    sprintf(command, "cat /proc/%d/stat | awk '{print $10}'>>log.txt\n", pid);
    system(command);
    
    sprintf(command, "echo Number of major faults, requiring page load from disk: >> log.txt\n");
    system(command);
    sprintf(command, "cat /proc/%d/stat | awk '{print $12}'>>log.txt\n", pid);
    system(command);
    
    
    printf("exiting program\n %d clients connected, %d guessed correctly\n", num_threads, num_c_guess);
    fflush(stdout);

    
    fprintf(fp_log,"Server exiting\n");
    fclose(fp_log);
    
    exit(0);
}

