#include "client.h"


/*functions to make:
check user input for length and
hello world
chars*/


int main(int argc, char * argv[]) {
    

    if (argc < 3) {
        printf("usage: client < ip address >\n");
        exit(1);
    }
    
    client_setup(argv[1], argv[2]);

    return 0;
}



/*Sets up the client side of the connection
and then takes guesses from the client*/
void client_setup(char* serv_addr, char* port){
    
    printf("Mastermind client\n");
    struct hostent *hp;
    struct sockaddr_in addr;
    int serv_port = atoi(port);
    int sockfd, ret;
    char buffer[BUF_SIZE];
    char * serverAddr;
    char * host;
    
    pthread_t rThread;
    
    serverAddr = serv_addr;


    host = serv_addr;
    hp = gethostbyname(host);
    bzero( (char *)&addr,sizeof(addr));
	addr.sin_family = AF_INET;
	bcopy(hp->h_addr, (char *)&addr.sin_addr, hp->h_length);
	addr.sin_port =htons(serv_port);
    

    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("error creating socket");
        exit(1);
    }

    
    ret = connect(sockfd, (struct sockaddr * ) &addr, sizeof(addr));
    if (ret < 0) {
        printf("Error connecting to the server!\n");
        exit(1);
    }
    memset(buffer, 0, BUF_SIZE);
    printf("Enter your messages one by one and press return key!\n");

    //creating a new thread for receiving messages from the server
    ret = pthread_create( & rThread, NULL, receive_message, (void * ) sockfd);
    if (ret) {
        printf("ERROR: Return Code from pthread_create() is %d\n", ret);
        exit(1);
    }

    //the client will only send a string of len 4
    while (fgets(buffer, BUF_SIZE, stdin) != NULL) {
        
        if(valid_code(buffer)!=1){
            printf("Please enter a valid code guess\n");
            continue;
            //if(strlen(buffer)>4)continue;
        }
        ret = write(sockfd, buffer, BUF_SIZE);
        if (ret < 0) {
            printf("Error sending data!\n\t-%s", buffer);
        }
        bzero(buffer, BUF_SIZE);
    }

}


/*Worker function, receives and prints 
the server messages*/
void * receive_message(void * socket) {
    int sockfd, ret;
    char buff[BUF_SIZE];
    
    sockfd = (int) socket;
    memset(buff, 0, BUF_SIZE);
    ret = read(sockfd, buff, MSG_BUF);
    printf("server: ");
    printf("%s\n", buff);
    
    while(1) {
        
        memset(buff, 0, BUF_SIZE);
        ret = read(sockfd, buff, BUF_SIZE);
        if (ret < 0) {
            printf("Error receiving data, client closing!\n");
            close(sockfd);
            exit(1);
            break;
        }
        if(game_state(buff)==-1){
            printf("%s", buff);
            fflush(stdout);
            close(sockfd);
            exit(0);
        }
        else if(game_state(buff)==0) {
            if(strlen(buff)==0){
                break;
            }
            printf("Server: ");
            printf("%s\n", buff);
            
        }
        else if(game_state(buff)==1){
            
            printf("%s\n",buff);
            close(sockfd);
            exit(0);
        }
        
    }
    close(sockfd);
    return NULL;
}

/*checks the server message to 
    see if game is ongoing or finished, 
    given the input
    */

int game_state(char* input){
    if(strstr(input, "SUCCESS")!=NULL){
        return 1;
    }
    else if(strstr(input, "FAILURE")!=NULL){
        return -1;
    }
    return 0;   
}


