//#pragma once
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>


char* gen_code();
int valid_char(char a);
int valid_code(char* code);
int check_code(char* input, char* output, char* code);
int char_pos(char* input, char* code);
int char_in(char* input, char* code);


#define CODE_SIZE 4
#define BUF_SIZE 100