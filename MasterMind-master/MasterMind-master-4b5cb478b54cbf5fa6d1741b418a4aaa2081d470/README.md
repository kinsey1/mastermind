----MasterMind-----
This is a client server game created by Kinsey Reeves,
based on the code breaking board game (look it up)
the server can handle multiple simultaneous clients
and will log their moves. 

Usage: 

Run the server 
./server <port no>

Run a client 
./client <serverIP> <port no>
*server IP can be 'localhost' or a 32bit IPv4


The server will sever the connection with a client if it 
takes too many guesses or guesses correctly. 
This program may be edited freely
