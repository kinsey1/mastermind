#include <stdio.h>
#include <signal.h>

void inter_handle(){
    int pid=getpid();
    printf("program terminating");
    printf("\nProcess interrupt %d:\n", pid);
    exit(0);
}




int main(){
    int pid=getpid();
    printf("Process %d: I am Process %d!\n", pid, pid);
	printf("Process %d: Just try and stop me!\n", pid);
    
    signal(SIGINT, inter_handle);
    
    while(1)sleep(1);
    
    
    return 0;
}