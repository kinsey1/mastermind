#include <stdio.h>
#include <pthread.h>

pthread_mutex_t flip_done;


pthread_t thread[2];
int flip;

static void* thread1(void* _){
    
    int a = 0;
    while(a<5){
        a++;
        sleep(1);
        printf("hello from thread 1\n");
        fflush(stdout);
    }
    flip =5;
    pthread_mutex_unlock(&flip_done);
    
}


static void* thread2(void* _){
    
    pthread_mutex_lock(&flip_done);
    flip = 2;
    
    int i= 0;
    while(i<5){
        i++;
        sleep(1);
        printf("hello from thread 2\n");
        fflush(stdout);
    }
    
    printf("thread 2: flipped coin %d\n",flip);
}

int main(int argc, char* argv[]){
    
    pthread_mutex_init(&flip_done,NULL);
    pthread_mutex_lock(&flip_done);
    pthread_create(&thread[1],NULL,thread2,NULL);
    pthread_create(&thread[0],NULL,thread1,NULL);
    
    pthread_mutex_destroy(&flip_done);
    
    
    pthread_exit(NULL);
    return 0;
}

